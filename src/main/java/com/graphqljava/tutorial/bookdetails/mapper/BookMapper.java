package com.graphqljava.tutorial.bookdetails.mapper;

import com.graphqljava.tutorial.bookdetails.entity.BookEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface BookMapper {

    @Insert("INSERT INTO book (name, page_count) "
            + "VALUES (#{name}, #{page_count})")
    @Options(useGeneratedKeys=true, keyProperty = "id", keyColumn = "id")
    void save(BookEntity entity);

    @Select("SELECT * FROM book b " +
            "WHERE b.id = #{id}")
    @Results(value = {
        @Result(property = "author", column = "author_id", one = @One(select = "com.graphqljava.tutorial.bookdetails.mapper.AuthorMapper.getById"))
    })
    BookEntity getById(@Param("id") Long id);

    @Update("UPDATE book "
            + "SET name = #{name}, page_count = #{page_count} "
            + "WHERE id = #{id}")
    void update(BookEntity entity);

    @Delete("DELETE FROM book WHERE id =#{id}")
    void deleteById(@Param("id") Long id);

    @Select("SELECT * FROM book b")
    @Results(value = {
            @Result(property = "author", column = "author_id", one = @One(select = "com.graphqljava.tutorial.bookdetails.mapper.AuthorMapper.getById"))
    })
    List<BookEntity> getAll();

    @Select("SELECT * FROM book b " +
            "WHERE b.name = #{name}")
    @Results(value = {
            @Result(property = "author", column = "author_id", one = @One(select = "com.graphqljava.tutorial.bookdetails.mapper.AuthorMapper.getById"))
    })
    BookEntity getByName(@Param("name") String name);

}
