CREATE TABLE IF NOT EXISTS `author` (
	`id`	    BIGINT PRIMARY KEY AUTO_INCREMENT,
	`first_name`	VARCHAR(100) NOT NULL,
    `last_name`	VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS `book` (
  	`id`	    BIGINT PRIMARY KEY AUTO_INCREMENT,
  	`name`      VARCHAR(100) NOT NULL,
    `page_count`	INT NOT NULL,
    `author_id`  BIGINT,

    CONSTRAINT fk_category
    FOREIGN KEY (author_id)
    REFERENCES author(id)
);