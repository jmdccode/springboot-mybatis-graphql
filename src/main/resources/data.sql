INSERT INTO author (first_name, last_name) VALUES
    ('Joanne', 'Rowling'),
    ('Herman',  'Melville'),
    ('Anne', 'Rice');

INSERT INTO book (name, page_count, author_id) VALUES
    ('Harry Potter', '223', 1),
    ('Moby Dick', '635', 2),
    ('Interview with the vampire', '371', 3);