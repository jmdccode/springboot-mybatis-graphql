package com.graphqljava.tutorial.bookdetails;

import com.graphqljava.tutorial.bookdetails.entity.BookEntity;
import com.graphqljava.tutorial.bookdetails.mapper.AuthorMapper;
import com.graphqljava.tutorial.bookdetails.mapper.BookMapper;
import graphql.schema.DataFetcher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GraphQLDataFetchers {

    @Autowired
    private BookMapper bookMapper;

    @Autowired
    private AuthorMapper authorMapper;

    public DataFetcher getBookByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            BookEntity book = bookMapper.getById(Long.valueOf(dataFetchingEnvironment.getArgument("id")));
            return book;
        };
    }

    public DataFetcher getBookByNameDataFetcher() {
        return dataFetchingEnvironment -> {
            BookEntity book = bookMapper.getByName(dataFetchingEnvironment.getArgument("name"));
            return book;
        };
    }

}
