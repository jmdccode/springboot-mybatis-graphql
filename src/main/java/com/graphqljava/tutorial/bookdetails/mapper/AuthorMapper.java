package com.graphqljava.tutorial.bookdetails.mapper;

import com.graphqljava.tutorial.bookdetails.entity.AuthorEntity;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface AuthorMapper {

    @Results({
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name")
    })

    @Insert("INSERT INTO author (first_name, last_name) "
            + "VALUES (#{first_name}, #{last_name})")
    @Options(useGeneratedKeys=true, keyProperty = "id", keyColumn = "id")
    void save(AuthorEntity entity);

    @Select("SELECT * FROM author WHERE ID = #{id}")
    AuthorEntity getById(@Param("id") Long id);

    @Update("UPDATE author "
            + "SET first_name = #{first_name}, last_name = #{last_name} "
            + "WHERE id = #{id}")
    void update(AuthorEntity entity);

    @Delete("DELETE FROM author WHERE id =#{id}")
    void deleteById(@Param("id") Long id);

    @Select("SELECT * FROM author")
    List<AuthorEntity> getAll();

}
